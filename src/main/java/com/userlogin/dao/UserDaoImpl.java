package com.userlogin.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.userlogin.beans.UserDetails;
import com.userlogin.model.User;

@Repository
public class UserDaoImpl implements UserDao {

	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
	public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
		this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
	}
	
	@Override
	public User findByName(String name) {
		
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("name", name);
        
		String sql = "SELECT * FROM users WHERE name=:name";
		
        User result = namedParameterJdbcTemplate.queryForObject(
                    sql,
                    params,
                    new UserMapper());
                    
        //new BeanPropertyRowMapper(Customer.class));
        
        return result;
        
	}

	@Override
	public List<User> findAll() {
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		String sql = "SELECT * FROM users";
		
        List<User> result = namedParameterJdbcTemplate.query(sql, params, new UserMapper());
       
        
        return result;
        
	}
	

	public Integer getId() {
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		String sql = "SELECT MAX(id) FROM users";
		
        List<Integer> result = namedParameterJdbcTemplate.query(sql, params, new IntMapper());
       
        
        return result.get(0);
        
	}
	
	
	private static final class IntMapper implements RowMapper<Integer> {

		public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
			Integer maxId = rs.getInt("id");
			return maxId;
		}
	}
	
	public Integer insertUser(UserDetails userdetail) {
		String query = "INSERT INTO users (id, name, email,createtimestamp,logintimestamp) VALUES (:id,:name,:email,:createtimestamp,:logintimestamp)";
		Map namedParameters = new HashMap();
		int newId = getId()+1;
		namedParameters.put("id",  newId);
		namedParameters.put("name", userdetail.getName());
		namedParameters.put("email", userdetail.getEmailId());
		Date tempDate = new Date();
		namedParameters.put("createtimestamp",tempDate.getTime());
		namedParameters.put("logintimestamp", tempDate.getTime());
		namedParameterJdbcTemplate.update(query, namedParameters);
		return newId;
	}
	
	public void loginUser(int id) {
		String query = "UPDATE users set logintimestamp = :logintimestamp where id = :id";
		Map namedParameters = new HashMap();
		namedParameters.put("id",  id);
		Date tempDate = new Date();
		namedParameters.put("logintimestamp", tempDate.getTime());
		namedParameterJdbcTemplate.update(query, namedParameters);
	}

	private static final class UserMapper implements RowMapper<User> {

		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setId(rs.getInt("id"));
			user.setName(rs.getString("name"));
			user.setEmail(rs.getString("email"));
			user.setCreateTimeStamp(rs.getLong("createtimestamp"));
			user.setLoginTimeStamp(rs.getLong("logintimestamp"));
			return user;
		}
	}

	@Override
	public User findById(int id) {
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);
        
		String sql = "SELECT * FROM users WHERE id=:id";
		
        User result = namedParameterJdbcTemplate.queryForObject(
                    sql,
                    params,
                    new UserMapper());
                    
        //new BeanPropertyRowMapper(Customer.class));
        
        return result;
	}

}