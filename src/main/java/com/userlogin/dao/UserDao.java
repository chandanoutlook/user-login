package com.userlogin.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.userlogin.beans.UserDetails;
import com.userlogin.model.User;

public interface UserDao {

	User findByName(String name);
	
	List<User> findAll();
	Integer insertUser(UserDetails userdetail);
	void loginUser(int id);
	User findById(int id);


}