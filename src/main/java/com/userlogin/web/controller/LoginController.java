package com.userlogin.web.controller;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.userlogin.beans.UserDetails;
import com.userlogin.dao.UserDao;
import com.userlogin.model.User;

@Controller
public class LoginController {

	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	UserDao userDao;
	
	
	
	@RequestMapping(value = "/users/create", method = RequestMethod.POST)

	public String create(@ModelAttribute("userForm") @Validated UserDetails user,Model model) {

		logger.debug("creating user for -"+user.getName());
		int id = userDao.insertUser(user);
		System.out.println(id);
		model.addAttribute("id", id);
		return "Creted User";

	}
	
	
	@RequestMapping(value = "/users/login/{id}", method = RequestMethod.GET)

	public String getLogin(@PathVariable("id") int id,Model model) {

		logger.debug("geting login link for user id  -"+id);
			
		String loginId = ""; 
		User user = userDao.findById(id);
     
		if (user != null && user.getId() == id){
		   userDao.loginUser(id);
		   loginId = loginId + "http://localhost:8080/users/link/"+id;
			String smtpHostServer = "smtp.userlogin.com";
			Properties props = System.getProperties();
		    props.put("mail.smtp.host", smtpHostServer);
		    Session session = Session.getInstance(props, null);
		    sendEmail(session, user.getEmail(),"Login Link", loginId);
			model.addAttribute("link", loginId);
		   return "Email Send to registered email id: "+user.getEmail();
		}else{
			model.addAttribute("link", loginId);
			return "Not a registerd user";
		}

	}
	
	
	@RequestMapping(value = "/users/link/{id}", method = RequestMethod.GET)

	public UserDetails getUserWithLink(@PathVariable("id") int id,Model model) {

		logger.debug("geting user details from link user id  -"+id);
		
		User user = userDao.findById(id);
		UserDetails userDetails = new UserDetails(); 
		if(user != null && user.getId() == id){
			long millis = 15 * 60 * 1000;
			Date date = new Date();
			long loginGap = date.getTime() - user.getLoginTimeStamp();
			if(millis  > loginGap){
				userDetails.setEmailId(user.getEmail());
				userDetails.setName(user.getName());
				model.addAttribute("user details", userDetails);
				model.addAttribute("login details", "successfully logedin");
				return userDetails;
			}else{
				model.addAttribute("login details", "login expired");
				return userDetails;
			}
		
		}else{
			model.addAttribute("login details", "wrong link");
			return userDetails;
		}
	}
	
	
	
	
	
	public static void sendEmail(Session session, String toEmail, String subject, String body){
		try
	    {
	      MimeMessage msg = new MimeMessage(session);
	      //set message headers
	      msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
	      msg.addHeader("format", "flowed");
	      msg.addHeader("Content-Transfer-Encoding", "8bit");

	      msg.setFrom(new InternetAddress("chandanpatro@test.com", "NoReply-JD"));

	      msg.setReplyTo(InternetAddress.parse("chandanpatro@test.com", false));

	      msg.setSubject(subject, "UTF-8");

	      msg.setText(body, "UTF-8");

	      msg.setSentDate(new Date());

	      msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
	      System.out.println("Message is ready");
    	  Transport.send(msg);  

	      System.out.println("EMail Sent Successfully!!");
	    }
	    catch (Exception e) {
	      e.printStackTrace();
	    }
	}

}