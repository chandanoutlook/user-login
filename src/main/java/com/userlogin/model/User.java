package com.userlogin.model;

import java.sql.Date;

public class User {

	Integer id;
	String name;
	String email;
	
	Long createTimeStamp;
	Long loginTimeStamp;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	public Long getCreateTimeStamp() {
		return createTimeStamp;
	}

	public void setCreateTimeStamp(Long createTimeStamp) {
		this.createTimeStamp = createTimeStamp;
	}

	public Long getLoginTimeStamp() {
		return loginTimeStamp;
	}

	public void setLoginTimeStamp(Long loginTimeStamp) {
		this.loginTimeStamp = loginTimeStamp;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", email=" + email + ", createTimeStamp=" + createTimeStamp
				+ ", loginTimeStamp=" + loginTimeStamp + "]";
	}

	

}